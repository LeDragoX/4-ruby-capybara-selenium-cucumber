# 4 Ruby Capybara Selenium Cubumber

Tópicos avançados 2: Selenium e Cucumber

- Login no acadêmico
- Coletar matérias do diário e printar

## First time requirements (Devs):

```sh
gpg --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
\curl -sSL https://get.rvm.io | bash -s stable --ruby
source ~/.rvm/scripts/rvm
\curl -sSL https://get.rvm.io | bash -s stable --rails
ruby -v # Check RUBY version
rvm -v  # Check RVM version
rvm requirements

# Using the latest version
#/bin/bash --login
rvm install 3.0.2
rvm use 3.0.2 --default
```

## Ruby project Requirements

```sh
# Install within the project directory
gem install bundler --no-document
gem install rspec
gem update

bundle install
bundle update

# Initialize RSPEC
rspec --init
# Initialize Cucumber
cucumber --init
```

## Selenium Web Driver requirements

Doc for each browser: https://www.selenium.dev/documentation/pt-br/webdriver/driver_requirements/

## Running manually the tests

```sh
# 1: liberar função main() e colocar dados reais para login
ruby src/user_academico.rb
# 2: Em features/user_academico.feature, colocar dados reais para login
cucumber
```

## Suggestion for webdrivers folder

```sh
mkdir ~/.config/web-drivers --parents                       # Download the drivers in here
echo $PATH                                                  # Choose one of the paths and put the files there
sudo cp ~/.config/web-drivers/* /usr/local/bin --recursive  # Will take chromedriver | geckodriver
export PATH="$PATH:~/.config/web-drivers/" >> ~/.profile    # Chrome driver
```
