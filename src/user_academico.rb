require 'selenium-webdriver'
# Selenium
class UserAcademico
  attr_accessor :user, :password
  attr_reader :browser

  def initialize(browser = :chrome)
    @browser = browser
  end
  
  def loginAluno(user, password)
    driver = Selenium::WebDriver.for @browser
    wait   = Selenium::WebDriver::Wait.new(timeout: 10)

    # Go to Alunos page
    driver.get 'https://academico.iff.edu.br/'
    driver.find_element(xpath: '//*[@id="marrom"]/tbody/tr[4]/td[2]/a').click()

    # Enter username and password
    driver.find_element(id: 'txtLogin').send_keys user
    driver.find_element(id: 'txtSenha').send_keys password, :return
    @driver = driver
  end
  
  def getSubjectList()
    # Get subject list
    @driver.find_element(xpath: '/html/body/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table/tbody/tr[5]/td[3]/a').click()
    subject_list = @driver.find_elements(xpath: '/html/body/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table[2]/tbody/tr/td[2]/p/table[2]/tbody/tr[*]/td[1]/strong')
    
    subject_list.each do |element|
      id_materia, periodo, nome_materia, professor = element.text().split(" - ")
      puts "#{id_materia}, #{periodo}, #{nome_materia}, #{professor}"
    end
  end
  
  def logout()
    # Log Out
    @driver.find_element(:xpath => '/html/body/table/tbody/tr[1]/td/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr/td[2]/div/a').click()
    
    sleep(3)  # Temp
    @driver.quit
  end
end

def main
  user1 = UserAcademico.new()
  user1.loginAluno('matricula', 'senha')
  user1.getSubjectList()
  user1.logout()
end

main()