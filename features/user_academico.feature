#language: en

Feature: Get Data
  In order to <login>
  As a <aluno>
  I want <login_aluno>

  # Esse cenário vai falhar sem login e senha real
  Scenario: I logged in successfully
    Given i enter the login page
    When i enter with "true_matricula" and "true_senha"
    Then i shall see the "Datas de provas,"

  Scenario: I failed to login
    Given i enter the login page
    When i enter with "fake_matricula" and "fake_senha"
    Then i shall see "Acesso Negado"