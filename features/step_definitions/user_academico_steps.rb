# Cucumber + Capybara + RSpec
Given('i enter the login page') do
  visit "https://academico.iff.edu.br/"
  find(:xpath, '//*[@id="marrom"]/tbody/tr[4]/td[2]/a').click()
end

When('i enter with {string} and {string}') do |username, password|
  within("form") do
    fill_in "LOGIN", with: username
    fill_in "SENHA", with: password
    click_on "OK"
  end
end

Then('i shall see the {string}') do |hello_msg|
  expect(page).to have_text hello_msg
  find(:xpath, '/html/body/table/tbody/tr[2]/td/table/tbody/tr[2]/td[2]/table/tbody/tr[5]/td[3]/a').click()

  subject_elements = all(".conteudoTexto > strong")
    
  subject_elements.each do |element|
    id_materia, periodo, nome_materia, professor = element.text().split(" - ")
    puts "#{id_materia}, #{periodo}, #{nome_materia}, #{professor}"
  end

end

Then('i shall see {string}') do |fail_msg|
  expect(page).to have_text fail_msg
end